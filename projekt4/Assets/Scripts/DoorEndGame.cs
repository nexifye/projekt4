using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorEndGame : MonoBehaviour
{
    public GameObject connectedDoor;
    public bool teleported = false;
    private GameObject player;
    public GameObject key;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    private void Update()
    {
        if (teleported && Input.GetAxisRaw("Open Door") < 1)
        {
            teleported = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (key.GetComponent<KeyScript>().playerHasKey == true)

                if (Input.GetAxisRaw("Open Door") == 1 && !teleported)
                {
                    Debug.Log("Loading level 2.");
                    Time.timeScale = 1f;
                    SceneManager.LoadScene("Level02");
                }
        }
    }
}
