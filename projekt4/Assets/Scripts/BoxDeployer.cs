// Skrypt na tworzenie sie schodow za pomoca ForLoop'a i Instantiate, ktore rozstawiaja w grze Prefab'y.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDeployer : MonoBehaviour
{
    // Zmienne.
    public GameObject obstaclePrefab;
    public int obstacleNum = 10;
    public float spawnDelay = 0.1f;
    public float obstacleHeighFactor = 1;
    public float obstacleWidthFactor = 1f;
    public Vector3 obstaclePosition;

    // Uruchomienie IEnumerator'a.
    void Start()
    {
        StartCoroutine(DeployObstacles());
    }

    // Respienie sie boxow / przeszkod za pomoca ForLoop'a.
    private IEnumerator DeployObstacles()
    {
        // ForLoop.
        for (int i = 0; i <= obstacleNum; i++)
        {
            // Pozycja respiacych sie boxow.
            obstaclePosition = new Vector3(obstaclePosition.x + obstacleWidthFactor, obstaclePosition.y + obstacleHeighFactor, obstaclePosition.z);
            // Tworzenie kopii prefab'a.
            GameObject obstacle = Instantiate(obstaclePrefab);
            // Wrzucenie prefab'ow do konkretnego folderu.
            obstacle.transform.parent = transform;
            obstacle.transform.position = obstaclePosition;
            // Opoznienie respienia boxow.
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}