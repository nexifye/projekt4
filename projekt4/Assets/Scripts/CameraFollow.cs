// Skrypt na podazanie kamery za graczem.
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Zmienne.
    public Transform target;
    public Vector3 offset;
    [Range(1, 10)]
    public float smoothFactor;

    // Uruchamianie skryptu.
    private void FixedUpdate()
    {
        Follow();
    }

    // Funkcjonowanie skryptu.
    void Follow()
    {
        Vector3 targetPosition = target.position + offset;
        Vector3 smoothPosition = Vector3.Lerp(transform.position, targetPosition, smoothFactor * Time.fixedDeltaTime);
        transform.position = targetPosition;
    }
}