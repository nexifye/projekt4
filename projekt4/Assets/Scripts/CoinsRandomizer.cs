// Skrypt na respienie sie randomowo coinsow. P�KI CO NIE DZIA�A NAJLEPIEJ, PRAWDOPODOBNIE JEST TO PROBLEM Z RANDOM.RANGE, RESPI SIE TONA COINSOW ALBO JEST TO PROBLEM Z SAMYM PREFABEM W UNITY.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsRandomizer : MonoBehaviour
{
    // Zmienne.
    public GameObject coinsPrefab;
    public int coinsNum = 10;
    public float spawnDelay = 0.1f;
    public float coinsHeight = 1;
    public float coinsWidthFactor = 1f;
    public Vector3 coinsPosition;
    public Vector3 coinsSize;

    // Uruchomienie IEnumerator'a.
    void Start()
    {
        StartCoroutine(DeployObstacles());
    }

    // Respienie sie coinsow za pomoca ForLoop'a.
    private IEnumerator DeployObstacles()
    {
        // ForLoop.
        for (int i = 0; i <= coinsNum; i++)
        {
            // Pozycja respiacych sie coinsow.
            coinsPosition = new Vector3(coinsPosition.x + Random.Range(-10.0f, 10.0f), coinsPosition.y + coinsHeight, coinsPosition.z + coinsHeight * coinsWidthFactor);
            // Tworzenie kopii prefab'a.
            GameObject coins = Instantiate(coinsPrefab, coinsPosition, Quaternion.identity);
            coins.transform.localScale = coinsSize;
            // Opoznienie respienia coinsow.
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}