// Skrypt na klucz, kt�ry otwiera drzwi.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    // Zmienne.
    public bool playerHasKey;
    private SpringJoint2D spring;

    // Funkcjonowanie skryptu.
    private void Start()
    {
        playerHasKey = false;
        spring = GetComponent<SpringJoint2D>();
        spring.enabled = false;
        GameObject backpack = GameObject.FindWithTag("Backpack");
        spring.connectedBody = backpack.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            spring.enabled = true;
            playerHasKey = true;
        }
    }
}
