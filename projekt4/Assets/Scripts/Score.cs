// Skrypt na zliczanie zebranych coinsow (Score)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    // Zmienne.
    public static Score instance;
    public Text scoreText;
    public Text highscoreText;
    public TMP_Text finalscoreText;
    int score = 0;
    public int highscore = 0;
    int finalscore = 0;

    private void Awake()
    {
        instance = this;
    }

    // Wyswietlanie sie tekstu.
    private void Start()
    {
        highscore = PlayerPrefs.GetInt("highscore", 0);
        scoreText.text = score.ToString() + " POINTS";
        highscoreText.text = "HIGHSCORE: " + highscore.ToString();
        finalscoreText.text = "YOU GOT " + finalscore.ToString() + " POINTS";
    }

    // Dodawanie punktow.
    public void AddPoint()
    {
        Debug.Log("Point added.");
        score += 1;
        finalscore += 1;
        scoreText.text = score.ToString() + " POINTS";
        finalscoreText.text = "YOU GOT " + finalscore.ToString() + " POINTS";
        if (highscore < score)
            PlayerPrefs.SetInt("highscore", score);
    }
}