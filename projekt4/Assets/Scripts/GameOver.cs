using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameOver : MonoBehaviour
{
    // Zmienne.
    public static bool GameIsPaused = false;
    public GameObject gameOverMenuUI;

    // Skrypt by gra nie uruchomila sie z wlaczonym PauseMenu.
    private void Start()
    {
        gameOverMenuUI.SetActive(false);
        GameIsPaused = false;
    }

    private void Update()
    {
    // Wczytanie level'a przez nacisniecie ESC.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Debug.Log("User pressed ESC, loading scene again.");
                //Time.timeScale = 1f;
                //SceneManager.LoadScene("Level01");
                int scene = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(scene, LoadSceneMode.Single);
                Time.timeScale = 1;
            }
        }
    }

    // Skrypt na wczytanie ponownie gry (Retry).
    public void Retry()
    {
        Debug.Log("Loading scene again.");
        //Time.timeScale = 1f;
        //SceneManager.LoadScene("Level01");
        GameIsPaused = false;
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
        Time.timeScale = 1f;
    }

    // Skrypt na pauze.
    public void EndGame()
    {
        Debug.Log("Opening game over menu.");
        gameOverMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        Destroy(FindObjectOfType<PauseMenu>().pauseMenuUI);
    }

    // Skrypt na zaladowanie sceny z Menu.
    public void MainMenu()
    {
        Debug.Log("Loading menu scene.");
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
        GameIsPaused = false;
    }
}
