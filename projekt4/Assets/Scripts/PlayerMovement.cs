// Movement gracza.
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    // Zmienne.
    public float movementSpeed;
    public Rigidbody2D rb;
    public float jumpForce = 20f;
    public Transform feet;
    public LayerMask groundLayers;
    public float mx;

    // Funkcjonowanie skryptu.
    private void Update()
    {
        mx = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            Jump();
        }
    }

    private void FixedUpdate()
    {
        Vector2 movement = new Vector2(mx * movementSpeed, rb.velocity.y);

        rb.velocity = movement;

        if (!Mathf.Approximately(0, mx))
            transform.rotation = mx > 0 ? Quaternion.Euler(0, -180, 0) : Quaternion.identity;

        if (rb.position.y <= 0.75f)
        {
            FindObjectOfType<GameOver>().EndGame();
            Destroy(FindObjectOfType<Score>().scoreText);
            Destroy(FindObjectOfType<Score>().highscoreText);
            Destroy(FindObjectOfType<JustVariables>().firepoint);
        }
    }

    void Jump()
    {
        Vector2 movement = new Vector2(rb.velocity.x, jumpForce);

        rb.velocity = movement;
    }

    public bool isGrounded()
    {
        Collider2D groundCheck = Physics2D.OverlapCircle(feet.position, 0.1f, groundLayers);

        if (groundCheck)
        {
            return true;
        }

        return false;
    }

    // Coins'y
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Coins"))
        {
            Destroy(col.gameObject);
            Score.instance.AddPoint();
        }

        else if (col.gameObject.name.Equals("Cloud"))
            this.transform.parent = col.transform;
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.name.Equals("Cloud"))
            this.transform.parent = null;
    }
}