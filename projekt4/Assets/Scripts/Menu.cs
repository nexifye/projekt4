// Skrypt menu (ladowanie leveli itd.)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Zaladowanie levelu 1.
    public void LoadLevel()
    {
        Debug.Log("Loading level.");
        Time.timeScale = 1f;
        SceneManager.LoadScene("Level01");
    }

    public void ExitGame()
    {
        Debug.Log("Exiting the game.");
        Application.Quit();
    }

    // Funkcjonowanie dropdown'u.
    public void HandleInputData(int val)
    {
        // Pierwszy przycisk - LEVELS
        if (val == 0)
        {
            Debug.Log("Choose level.");
            Time.timeScale = 1f;
        }

        // Drugi przycisk - level 1
        if (val == 1)
        {
            Debug.Log("Loading level 1.");
            Time.timeScale = 1f;
            SceneManager.LoadScene("Level01");
        }

        // Trzeci przycisk - level 2
        if (val == 2)
        {
            Debug.Log("Loading level 2.");
            Time.timeScale = 1f;
            SceneManager.LoadScene("Level02");
        }

        // Czwarty przycisk - level 3
        if (val == 3)
        {
            Debug.Log("Loading level 3.");
            Time.timeScale = 1f;
            SceneManager.LoadScene("Level03");
        }
    }
}