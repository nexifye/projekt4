using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour
{
    public int health = 100;
    public GameObject deathEffect;
    private bool isCoroutineExecuting = false;

    // Obrazenia.
    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Debug.Log("Player died.");
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        FindObjectOfType<GameOver>().EndGame();
    }
}
