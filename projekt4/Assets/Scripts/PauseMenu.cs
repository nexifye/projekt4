using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    // Zmienne
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;

    // Skrypt by gra nie uruchomila sie z wlaczonym PauseMenu.
    private void Start()
    {
        pauseMenuUI.SetActive(false);
        GameIsPaused = false;
    }

    // Przypisanie klawiszy.
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        // Restart.
        if (Input.GetKeyDown(KeyCode.F5))
        {
            Debug.Log("User pressed F5, game reloaded.");
            //Time.timeScale = 1f;
            //SceneManager.LoadScene("Level01");
            GameIsPaused = false;
            int scene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
            Time.timeScale = 1f;
        }
    }

    // Skrypt na wznowienie gry.
    public void Resume()
    {
        Debug.Log("Game resumed.");
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

    }

    // Skrypt na pauze.
    public void Pause()
    {
        Debug.Log("Opened pause menu.");
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    // Skrypt na zaladowanie sceny z Menu.
    public void MainMenu()
    {
        Debug.Log("Loading menu scene.");
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
        GameIsPaused = false;
    }
}
