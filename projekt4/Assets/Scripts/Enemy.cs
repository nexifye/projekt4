// Skrypt na AI przeciwnikow.
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Zmienne.
    public float speed;
    // public float stoppingDistance; distanceFromPlayer > stoppingDistance
    public float lineOfSight;
    public float shootingRange;
    public float fireRate = 1f;
    private float nextFireTime;
    public GameObject bullet;
    public GameObject bulletParent;
    public SpriteRenderer spriteRenderer;
    public Rigidbody2D rb;
    private Transform target;
    public int health = 100;
    public GameObject deathEffect;

    // Wyszukiwanie pozycji gracza.
    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Poruszanie sie przeciwnika i obracanie.
    private void Update()
    {
        float distanceFromPlayer = Vector2.Distance(transform.position, target.position);
        if (distanceFromPlayer < lineOfSight && distanceFromPlayer > shootingRange)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, target.position, speed * Time.deltaTime);
        }
        else if (distanceFromPlayer <= shootingRange && nextFireTime < Time.time)
        {
            Instantiate(bullet, bulletParent.transform.position, Quaternion.identity);
            nextFireTime = Time.time + fireRate;
        }
    }

    // Zasieg widzenia przeciwnika.
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSight);
        Gizmos.DrawWireSphere(transform.position, shootingRange);
    }

    // Obrazenia.
    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Debug.Log("Enemy died.");
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}