using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float speed = 20f;
    public int damage = 40;
    public Rigidbody2D rb;
    public GameObject impactEffect;
    public GameObject target;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player");
        Vector2 moveDir = (target.transform.position - transform.position).normalized * speed;
        rb.velocity = new Vector2(moveDir.x, moveDir.y);
        Destroy(this.gameObject, 2);
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        PlayerDamage target = hitInfo.GetComponent<PlayerDamage>();
        if (target != null)
        {
            target.TakeDamage(damage);
        }

        Instantiate(impactEffect, transform.position, transform.rotation);

        Debug.Log("Enemy hit " + hitInfo.name);
        Destroy(gameObject);
    }
    /*
        GameObject target;
        public float speed;
        Rigidbody2D bulletRB;

        private void Start()
        {
            bulletRB = GetComponent<Rigidbody2D>();
            target = GameObject.FindGameObjectWithTag("Player");
            Vector2 moveDir = (target.transform.position - transform.position).normalized * speed;
            bulletRB.velocity = new Vector2(moveDir.x, moveDir.y);
            Destroy(this.gameObject, 2);
        }
    */
}
